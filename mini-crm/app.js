var divapp = document.getElementById('app');

var requestSRC = './data/crm.json';

var request = new XMLHttpRequest();
request.open('GET', requestSRC, false);
request.send();
var baptiste = JSON.parse(request.responseText);


function populateDiv(jsonObj) {

  var template = `

    <div class ="carte">
        <div class ="partie1">
            <div class ="image">
               <img src ="{{picture}}">
            </div>
            <div class ="description">
                <h2>{{firts_name}} {{last_name}}</h2>
                <ul>
                    <li>{{title}}</li>
                    <li>{{birthday}}</li>
                    <li>{{email}}</li>
                    <li>{{phone}}</li>
                    <li>{{company}}</li>
                 </ul>
                <p>{{desciption}}</p>
            </div> 
          </div>
        <div class ="partie2">
            <div class ="notes">
                <h3>Notes</h3>
                    <ul>
                        {{#notes}}
                          <li>{{subject}}</li>
                          <li>{{note}}</li>
                          <li>{{related_to}}</li>
                        {{/notes}}
                    </ul>
            </div>
            <div class ="Tasks">
                <h3>Tasks</h3>
                    <ul>
                        {{#tasks}}
                          <li>{{task}}</li>
                          <li>{{category}}</li>
                          <li>{{date}}</li>
                          <li>{{owner}}</li>
                          <li>{{priority}}</li>
                          <li>{{status}}</li>
                          <li>{{related_to}}</li>
                          <li>{{related_deal}}</li>
                        {{/tasks}}
                    </ul>  
            </div>
        </div>
        <div class ="partie3">
            <div class ="tags">
                <h3>Tags</h3>
                    <p>
                        {{tags}}
                    </p>
            </div>
        </div>
    </div>`

    jsonObj.customers.forEach(function (perso) {
      Mustache.parse();
      var rendered = Mustache.render(template, perso);
      $('#app').append(rendered);
    });
};



populateDiv(baptiste);

  /*var divperso = document.createElement('div');
  divapp.appendChild(divperso);

  var myH1 = document.createElement('h1');
  myH1.textContent = jsonObj.customers[0].firts_name + " " + jsonObj.customers[0].last_name;
  divperso.appendChild(myH1);

  var img = document.createElement('img');
  divperso.appendChild(img);


  var ul = document.createElement('ul');
  divperso.appendChild(ul);

  var li1 = document.createElement('li');
  li1.textContent = jsonObj.customers[0].email;
  ul.appendChild(li1)

  var myPara = document.createElement('p');
  myPara.textContent = 'birthday : ' + (jsonObj.customers[0].birthday);
  divperso.appendChild(myPara);

}

request.onload = function () {
  var perso = request.response;
  populateDiv(perso);

}
*/
/*$.getJSON("data/crm.json",function (data){
  var company = document.createElement('h1');
  var company = data.customers[1].company
  divapp.appendChild(company);
  $("#app").append(company)
})*/